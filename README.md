# Sailfish OS on Oneplus Oneplus 3/3T (oneplus3)

![SailfishOS](https://sailfishos.org/wp-content/themes/sailfishos/icons/apple-touch-icon-120x120.png)

## Current Build
2.2.1.18 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/oneplus3-ci/badges/master/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/oneplus3-ci/commits/master)


